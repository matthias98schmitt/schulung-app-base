package de.schulung.app.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/person")
public class PersonController {

  @Autowired
  private PersonService personService;

  @GetMapping
  public ResponseEntity getPersons() {
    return ResponseEntity.status(HttpStatus.OK).body(personService.getPersons());
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity getPersonById(@PathVariable Long id) {
    Person person = personService.getPersonById(id);
    if(person == null) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
    return ResponseEntity.status(HttpStatus.OK).body(person);
  }

  @PostMapping
  public ResponseEntity createPerson(@RequestBody Person personToCreate) {
    Person person = personService.createPerson(personToCreate);
    if(person == null) {
      return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }
    return ResponseEntity.status(HttpStatus.CREATED).body(person);
  }


  @DeleteMapping(value = "/{id}")
  public ResponseEntity deletePerson(@PathVariable Long id) {
    Person person = personService.deletePerson(id);
    if(person == null) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
    return ResponseEntity.status(HttpStatus.ACCEPTED).build();
  }
}
