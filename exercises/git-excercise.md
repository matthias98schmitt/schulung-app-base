# Voraussetzungen
Git installiert

# Aufgabe 1 - Repository initialisieren und eine Datei committen
a) Initialsiere ein lokales Repository und Prüfe den Status deines Repository

b) Füge eine Markdown oder Textdatei (mit beliebigem Inhalt) hinzu Prüfe den Status nochmal

b) Füge die Datei deinem Repository hinzu, mit einer sinnvollen Commit Nachricht

# Aufgabe 2 - Branching
a) Erstelle eine Branch "develop" und prüfe nach der Erstellung ob sie existiert

b) Wechsle in branch develop und mache ein Änderung an der Datei

c) Lass dir den unterschied Zwischen der Datei im Repository und deinen Änderungen anzeigen

# Aufgabe 3 Cloning
Forke das Repository https://bit.ly/2RTfeaw und klone es